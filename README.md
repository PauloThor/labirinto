# Dungeon Maze

## About

Dungeon Maze is a maze game with RPG elements. The player can defeat monsters, progress his level, choose class and interact with items during the game. You path and decision making matters so think carefully before facing a monster or taking an item from the floor.

### Technologies

<li>HTML</li>
<li>CSS</li>
<li>Javascript</li>
<li>Bulma</li>

### Layout

<img src='https://i.im.ge/2021/08/22/WFSSK.png'>

<img src='https://i.im.ge/2021/08/22/WFdN9.png'>
